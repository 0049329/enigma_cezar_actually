using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;

namespace Enigma_Cezar_actually_
{
	class UserDataBase
	{
		public List<UserAccount> listOfAccounts { get; set; }

		public void CreateNewAccount(string pathOfEnigmaFolder)
		{
			UserAccount userAccount = new UserAccount();

			Console.WriteLine("Введите логин");

			string login = Console.ReadLine();

			userAccount.Login = login;

			Console.WriteLine("Введите пароль");

			string password = Console.ReadLine();

			userAccount.Password = password;

			Console.WriteLine("Введите Ваше имя");

			string userName = Console.ReadLine();

			userAccount.UserName = userName;

			bool accountAllreadyExist = false;

			foreach(UserAccount accountWhoWeTryingToAdd in listOfAccounts)
			{
				if(accountWhoWeTryingToAdd.Login == userAccount.Login)
				{
					accountAllreadyExist = true;
				}
			}

			if(accountAllreadyExist == false)
			{
				userAccount.pathToAccountFolder = ($"{pathOfEnigmaFolder}\\{userAccount.Login}");

				DirectoryInfo accountFolder = new DirectoryInfo(userAccount.pathToAccountFolder);

				if(!accountFolder.Exists)
				{
					accountFolder.Create();

					using (FileStream streamWhoJustCreatingFileOfListOfAccounts = new FileStream($"{pathOfEnigmaFolder}\\{userAccount.Login}\\ListOfUserInviteMessages.json", FileMode.OpenOrCreate)) ;

					listOfAccounts.Add(userAccount);
				}
				else
				{
					Console.WriteLine("Логин занят");
				}

			}
			else
			{
				Console.WriteLine("Логин занят");
			}
		}

		public (string, string, string, string) EnterInAccount()
		{
			Console.WriteLine("Введите логин");

			string checkLogin = Console.ReadLine();

			Console.WriteLine("Введите пароль");

			string checkPassword = Console.ReadLine();

			(string, string, string, string) result = ("", "", "", "");

			foreach(UserAccount userAccount in listOfAccounts)
			{
				if(userAccount.Login == checkLogin && userAccount.Password == checkPassword)
				{
					result = (userAccount.Login,
							  userAccount.Password,
							  userAccount.UserName,
							  userAccount.pathToAccountFolder);

					Console.WriteLine($"Вы вошли, как: {userAccount.UserName}");
				}
			}

			if (result == ("","","",""))
			{
				Console.WriteLine("Аккаунт не существует либо неправильные логин или пароль");
			}

			return result;
		}
	}

	class UserAccount
	{
		public string Login { get; set; }

		public string Password { get; set; }

		public string UserName { get; set; }

		public string pathToAccountFolder { get; set; }
	}

	class ListOfMessages
	{
		public List<UserMessage> listOfMessages { get; set; }
	}

	class UserMessage
	{
		public string TextOfMessage { get; set; }

		public string UserWhoSendMessage { get; set; }

		public string TimeWhenMessageSended { get; set; }
	}

	class ListOfUserInviteMessages
	{
		public List<UserInviteMessage> listOfUserInviteMessages { get; set; }
	}

	class UserInviteMessage : UserMessage
	{
		public string UserName { get; set; }
		public bool InviteAccepted { get; set; }
	}

	class KeyForEncrypto
	{
		public int keyForEncrypto { get; set; } = 0;

		public int SetKeyToEncrypto()
		{
			int keyToEncrypto = 0;

			Console.WriteLine("Введите ключ шифрования в диапазоне от 1 до 10");

			bool tryingToEnterOfKey = false;

			while (!tryingToEnterOfKey)
			{
				while (!int.TryParse(Console.ReadLine(), out keyToEncrypto))
				{
					Console.WriteLine("Вводите только цифры в диапазоне от 1 до 10");

				}

				if (keyToEncrypto >= 1 || keyToEncrypto <= 10)
				{
					tryingToEnterOfKey = true;
				}
			}

			return keyToEncrypto;
		}
	}

	class Program
	{
		static string CezarCrypt(string textForCrypting, int keyForCrypting)
		{
			char[] charedTextForCrypting = textForCrypting.ToCharArray();

			string alphabet = "           АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯяAaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz           ";

			for (int i = 0; i < charedTextForCrypting.Length; i++)
			{
				int currentAlphabetIndex = 0;

				for (int j = 5; j < alphabet.Length; j++)
				{
					if (charedTextForCrypting[i] == alphabet[j])
					{
						charedTextForCrypting[i] = alphabet[j + keyForCrypting];

						break;
					}

					currentAlphabetIndex++;
				}
			}

			string cryptedText = new string(charedTextForCrypting);

			return cryptedText;
		}

		static string CezarDecrypt(string cryptedText, int keyForDecrypting)
		{
			char[] charedTextForDeCrypting = cryptedText.ToCharArray();

			string alphabet = "           АаБбВвГгДдЕеЁёЖжЗзИиЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщЪъЫыЬьЭэЮюЯяAaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz           ";

			for (int i = 0; i < charedTextForDeCrypting.Length; i++)
			{
				int currentAlphabetIndex = 0;

				for (int j = 5; j < alphabet.Length; j++)
				{
					if (charedTextForDeCrypting[i] == alphabet[j])
					{
						charedTextForDeCrypting[i] = alphabet[j - keyForDecrypting];

						break;
					}

					currentAlphabetIndex++;
				}
			}

			string decryptedText = new string(charedTextForDeCrypting);

			return decryptedText;
		}
		static async Task Main(string[] args)
		{
			#region Creating Enigma Folder
			string pathOfEnigmaFolder = "Enigma folder";

			DirectoryInfo enigmaDirectory = new DirectoryInfo(pathOfEnigmaFolder);

			if(!enigmaDirectory.Exists)
			{
				enigmaDirectory.Create();
			}
			#endregion

			#region Load List Of User Account
			UserDataBase userDataBase = new UserDataBase()
			{
				listOfAccounts = new List<UserAccount>()
			};

			UserAccount currentUser = new UserAccount();

			using (FileStream streamWhoJustCreatingFileOfListOfAccounts = new FileStream($"{pathOfEnigmaFolder}\\ListOfUserAccounts.json", FileMode.OpenOrCreate));

			string[] fileIsEmpty = File.ReadAllLines($"{pathOfEnigmaFolder}\\ListOfUserAccounts.json");

			if(fileIsEmpty.Length != 0)
			{
				using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\ListOfUserAccounts.json", FileMode.OpenOrCreate))
				{
					UserDataBase restoredUserDataBase = await JsonSerializer.DeserializeAsync<UserDataBase>(fileStream);

					UserAccount restoredUserAccount = new UserAccount();

					foreach(UserAccount userAccount in restoredUserDataBase.listOfAccounts)
					{
						restoredUserAccount = userAccount;

						userDataBase.listOfAccounts.Add(userAccount);
					}
				}
			}

			#endregion

			Console.WriteLine("Добро пожаловать в чат шифруемый Enigma(Cezar)");

			bool exitFlag = false;

			while(!exitFlag)
			{
				Console.WriteLine("1 - для создания аккаунта");

				Console.WriteLine("2 - для входа в аккаунт");

				Console.WriteLine("3 - для закрытия чата");

				Console.WriteLine("4 - для очистки консоли");

				int switcher = 0;

				while(!int.TryParse(Console.ReadLine(), out switcher))
				{
					Console.WriteLine("Для навигации вводите только цифры 1, 2 или 3");

					Console.WriteLine("1 - для создания аккаунта");

					Console.WriteLine("2 - для входа в аккаунт");

					Console.WriteLine("3 - для закрытия чата");

					Console.WriteLine("4 - для очистки консоли");
				}

				switch(switcher)
				{
					case 1:
						{
							userDataBase.CreateNewAccount(pathOfEnigmaFolder);

							break;
						}

					case 2:
						{
							var currentAccount = userDataBase.EnterInAccount();

							ListOfUserInviteMessages listOfUserInviteMessages = new ListOfUserInviteMessages()
							{
								listOfUserInviteMessages = new List<UserInviteMessage>()
							};

							KeyForEncrypto currentUserKeyForEncrypto = new KeyForEncrypto();

							using (FileStream streamWhoCreateFile = new FileStream($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\UserKeyForEncrypto.json", FileMode.OpenOrCreate));

							fileIsEmpty = File.ReadAllLines($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\UserKeyForEncrypto.json");

							if(fileIsEmpty.Length != 0)
							{
								using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\UserKeyForEncrypto.json", FileMode.OpenOrCreate))
								{
									KeyForEncrypto restoredCurrentUserKeyForEncrypto = await JsonSerializer.DeserializeAsync<KeyForEncrypto>(fileStream);

									currentUserKeyForEncrypto = restoredCurrentUserKeyForEncrypto;
								}
							}

							using (FileStream streamWhoCreateFile = new FileStream($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\ListOfUserInviteMessages.json", FileMode.OpenOrCreate));

							fileIsEmpty = File.ReadAllLines($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\ListOfUserInviteMessages.json");

							if (fileIsEmpty.Length != 0)
							{
								using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\ListOfUserInviteMessages.json", FileMode.OpenOrCreate))
								{
									ListOfUserInviteMessages restoredListOfUserInviteMessages = await JsonSerializer.DeserializeAsync<ListOfUserInviteMessages>(fileStream);

									UserInviteMessage restoredUserInviteMessage = new UserInviteMessage();

									foreach (UserInviteMessage userInviteMessage in restoredListOfUserInviteMessages.listOfUserInviteMessages)
									{
										restoredUserInviteMessage = userInviteMessage;

										listOfUserInviteMessages.listOfUserInviteMessages.Add(restoredUserInviteMessage);
									}
								}
							}

							if (currentAccount.Item1 != "")
							{
								bool flagForExitFromAccount = false;

								while(!flagForExitFromAccount)
								{
									Console.WriteLine("1 - для перехода к списку возможных собеседников");

									Console.WriteLine("2 - для просмотра списка приглашений");

									Console.WriteLine("3 - для выхода из аккаунта");

									switcher = 0;

									while (!int.TryParse(Console.ReadLine(), out switcher))
									{
										Console.WriteLine("Для навигации вводите только цифры 1, 2 или 3");

										Console.WriteLine("1 - для перехода к списку возможных собеседников");

										Console.WriteLine("2 - для просмотра списка приглашений");

										Console.WriteLine("3 - для выхода из аккаунта");
									}

									switch(switcher)
									{
										case 1:
											{
												Console.WriteLine("Вы можете написать следующим пользователям");

												int counter = 0;

												foreach (UserAccount userAccount in userDataBase.listOfAccounts)
												{

													Console.WriteLine(counter + " " + userAccount.Login);

													counter++;

												}

												bool flagForExitFromChatList = false;

												while(!flagForExitFromChatList)
												{
													Console.WriteLine("Введите номер пользователя, чтобы предложить ему завязать беседу");

													int usersChoiceOfCommunicate = int.MaxValue;

													bool tryingToEnterUsersNumber = false;

													while(!tryingToEnterUsersNumber)
													{
														while (!int.TryParse(Console.ReadLine(), out usersChoiceOfCommunicate))
														{
															Console.WriteLine("Вводите только цифры");
														}

														if(usersChoiceOfCommunicate >= 0 || usersChoiceOfCommunicate < userDataBase.listOfAccounts.Count)
														{
															tryingToEnterUsersNumber = true;
														}
														else
														{
															Console.WriteLine("Попробуйте еще раз ввести номер собеседника");
														}
													}

													bool inviteIsAccepted = false;

													foreach(UserInviteMessage usersCompanionInviteMessage in listOfUserInviteMessages.listOfUserInviteMessages)
													{
														if(userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login == usersCompanionInviteMessage.UserName)
														{
															if(usersCompanionInviteMessage.InviteAccepted == true)
															{
																inviteIsAccepted = true;
															}
														}
													}

													fileIsEmpty = File.ReadAllLines($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\ListOfUserInviteMessages.json");

													if (fileIsEmpty.Length != 0)
													{
														using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\ListOfUserInviteMessages.json", FileMode.OpenOrCreate))
														{
															ListOfUserInviteMessages restoredListOfUserInviteMessages = await JsonSerializer.DeserializeAsync<ListOfUserInviteMessages>(fileStream);

															UserInviteMessage restoredUserInviteMessage = new UserInviteMessage();

															foreach (UserInviteMessage userInviteMessage in restoredListOfUserInviteMessages.listOfUserInviteMessages)
															{
																restoredUserInviteMessage = userInviteMessage;

																listOfUserInviteMessages.listOfUserInviteMessages.Add(restoredUserInviteMessage);
															}
														}
													}

													foreach(UserInviteMessage userInviteMessage in listOfUserInviteMessages.listOfUserInviteMessages)
													{
														if(userInviteMessage.UserName == currentAccount.Item1)
														{
															if (userInviteMessage.InviteAccepted)
															{
																inviteIsAccepted = true;
															}
														}
													}

													if (inviteIsAccepted)
													{
														bool flagForExitFromChat = false;

														while (!flagForExitFromChat)
														{
															Console.WriteLine($"Проверка пути - {pathOfEnigmaFolder}\\{currentAccount.Item1}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}");

															DirectoryInfo currentUserDirectory = new DirectoryInfo($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}");

															if (!currentUserDirectory.Exists)
															{
																currentUserDirectory.Create();
															}

															DirectoryInfo usersCompanionOfChatingDirectory = new DirectoryInfo($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\{currentAccount.Item1}");

															if (!usersCompanionOfChatingDirectory.Exists)
															{
																usersCompanionOfChatingDirectory.Create();
															}

															using (FileStream streamWhoCreatingFileOfListOfMessages = new FileStream($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\listOfMessages.json", FileMode.OpenOrCreate)) ;

															fileIsEmpty = File.ReadAllLines($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\listOfMessages.json");

															ListOfMessages listOfCurrentAccountMessages = new ListOfMessages()
															{
																listOfMessages = new List<UserMessage>()
															};

															if (fileIsEmpty.Length != 0)
															{
																using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\listOfMessages.json", FileMode.OpenOrCreate))
																{
																	ListOfMessages restoredListOfMessages = await JsonSerializer.DeserializeAsync<ListOfMessages>(fileStream);

																	UserMessage restoredUserMessage = new UserMessage();

																	foreach (UserMessage userMessage in restoredListOfMessages.listOfMessages)
																	{
																		restoredUserMessage = userMessage;

																		restoredUserMessage.TextOfMessage = CezarDecrypt(restoredUserMessage.TextOfMessage, currentUserKeyForEncrypto.keyForEncrypto);

																		listOfCurrentAccountMessages.listOfMessages.Add(restoredUserMessage);
																	}
																}
															}

															using (FileStream streamWhoCreatingFileOfListOfMessages = new FileStream($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\{currentAccount.Item1}\\listOfMessages.json", FileMode.OpenOrCreate)) ;

															fileIsEmpty = File.ReadAllLines($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\{currentAccount.Item1}\\listOfMessages.json");

															ListOfMessages listOfCompanionMessages = new ListOfMessages()
															{
																listOfMessages = new List<UserMessage>()
															};

															if (fileIsEmpty.Length != 0)
															{
																using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\{currentAccount.Item1}\\listOfMessages.json", FileMode.OpenOrCreate))
																{
																	ListOfMessages restoredListOfMessages = await JsonSerializer.DeserializeAsync<ListOfMessages>(fileStream);

																	UserMessage restoredCompanionMessage = new UserMessage();

																	foreach (UserMessage companionMessage in restoredListOfMessages.listOfMessages)
																	{
																		restoredCompanionMessage = companionMessage;

																		restoredCompanionMessage.TextOfMessage = CezarDecrypt(restoredCompanionMessage.TextOfMessage, currentUserKeyForEncrypto.keyForEncrypto);

																		listOfCompanionMessages.listOfMessages.Add(restoredCompanionMessage);
																	}
																}
															}

															

															Console.WriteLine("Когда захотите покинуть чат, введите слово 'выход'");

															Console.WriteLine("Если захотите удалить какое-либо ссобщение, то введите 'удалить'");

															Console.WriteLine("Нажмите любую кнопку, чтобы перейти к чату");

															Console.ReadKey();

															while (!flagForExitFromChat)
															{
																Console.Clear();


																int numberOfMessage = 0;

																foreach (UserMessage userMessage in listOfCurrentAccountMessages.listOfMessages)
																{
																	Console.WriteLine(numberOfMessage + " " + userMessage.TextOfMessage + " от " + userMessage.UserWhoSendMessage +  " " + userMessage.TimeWhenMessageSended);

																	numberOfMessage++;
																}

																string message = "";

																message = Console.ReadLine();

																if (message != "выход" && message != "удалить")
																{
																	UserMessage userMessage = new UserMessage();

																	userMessage.TextOfMessage = message;

																	userMessage.UserWhoSendMessage = currentAccount.Item1;

																	userMessage.TimeWhenMessageSended = DateTime.Now.ToString();

																	listOfCurrentAccountMessages.listOfMessages.Add(userMessage);

																	listOfCompanionMessages.listOfMessages.Add(userMessage);
																}
																if(message == "удалить")
																{
																	if(listOfCurrentAccountMessages.listOfMessages.Count != 0)
																	{
																		Console.WriteLine("Введите номер сообщения, которое хотите удалить");

																		int numberOfDeletingMessage = 0;

																		bool tryingToEnterNumberOfDeletingMessage = false;

																		while (!tryingToEnterNumberOfDeletingMessage)
																		{
																			while (!int.TryParse(Console.ReadLine(), out numberOfDeletingMessage))
																			{
																				Console.WriteLine("Вводите только номера сообщений");
																			}

																			if (numberOfDeletingMessage < listOfCurrentAccountMessages.listOfMessages.Count || numberOfDeletingMessage >= 0)
																			{
																				tryingToEnterNumberOfDeletingMessage = true;
																			}
																		}

																		listOfCurrentAccountMessages.listOfMessages.RemoveAt(numberOfDeletingMessage);

																		listOfCompanionMessages.listOfMessages.RemoveAt(numberOfDeletingMessage);
																	}
																	else
																	{
																		Console.WriteLine("Чат пуст");
																	}
																}
																if (message == "выход")
																{
																	foreach (UserMessage userMessage in listOfCurrentAccountMessages.listOfMessages)
																	{
																		userMessage.TextOfMessage = CezarCrypt(userMessage.TextOfMessage, currentUserKeyForEncrypto.keyForEncrypto);

																		Console.WriteLine(userMessage.TextOfMessage);
																	}

																	listOfCompanionMessages = listOfCurrentAccountMessages;

																	using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\listOfMessages.json", FileMode.OpenOrCreate))
																	{
																		await JsonSerializer.SerializeAsync<ListOfMessages>(fileStream, listOfCurrentAccountMessages);
																	}

																	using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\{currentAccount.Item1}\\listOfMessages.json", FileMode.OpenOrCreate))
																	{
																		await JsonSerializer.SerializeAsync<ListOfMessages>(fileStream, listOfCompanionMessages);
																	}

																	flagForExitFromChat = true;
																}
															}

															flagForExitFromChat = true;
														}
													}
													else
													{
														bool inviteMessageSended = false;

														foreach(UserInviteMessage inviteMessage in listOfUserInviteMessages.listOfUserInviteMessages)
														{
															if(inviteMessage.UserName == currentAccount.Item1)
															{
																inviteMessageSended = true;
															}
														}

														if(inviteMessageSended)
														{
															Console.WriteLine("Вы уже отправили сообщение. Дождитесь ответа от собеседника");
														}
														else if(inviteMessageSended == false)
														{
															UserInviteMessage userInviteMessage = new UserInviteMessage();

															userInviteMessage.InviteAccepted = false;

															userInviteMessage.TextOfMessage = $"Пользователь {currentAccount.Item1} приглашает в чат";

															userInviteMessage.UserName = currentAccount.Item1;

															listOfUserInviteMessages.listOfUserInviteMessages.Add(userInviteMessage);

															currentUserKeyForEncrypto.keyForEncrypto = currentUserKeyForEncrypto.SetKeyToEncrypto();

															Console.WriteLine("Приглашение сформировано и отправлено. Ждем ответа от собеседника");

															using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\ListOfUserInviteMessages.json", FileMode.OpenOrCreate))
															{
																await JsonSerializer.SerializeAsync<ListOfUserInviteMessages>(fileStream, listOfUserInviteMessages);
															}

															using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{currentAccount.Item1}\\UserKeyForEncrypto.json", FileMode.OpenOrCreate))
															{
																await JsonSerializer.SerializeAsync<KeyForEncrypto>(fileStream, currentUserKeyForEncrypto);
															}

															using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{userDataBase.listOfAccounts[usersChoiceOfCommunicate].Login}\\UserKeyForEncrypto.json", FileMode.OpenOrCreate))
															{
																await JsonSerializer.SerializeAsync<KeyForEncrypto>(fileStream, currentUserKeyForEncrypto);
															}

														}														
													}
													
													flagForExitFromChatList = true;
												}

												break;
											}

										case 2:
											{
												if(listOfUserInviteMessages.listOfUserInviteMessages.Count != 0)
												{
													int counter = 0;

													foreach (UserInviteMessage userInviteMessage in listOfUserInviteMessages.listOfUserInviteMessages)
													{
														if (userInviteMessage.InviteAccepted)
														{
															Console.WriteLine(counter + " " + userInviteMessage.TextOfMessage + "принято");
														}
														else if(userInviteMessage.InviteAccepted == false)
														{
															Console.WriteLine(counter + " " + userInviteMessage.TextOfMessage);
														}

														counter++;
													}

													bool flagForExitFromInviteAccepting = false;

													while (!flagForExitFromInviteAccepting)
													{
														Console.WriteLine("Введите номер приглашения для того, чтобы принять его");

														switcher = 0;

														bool tryingToSwitchInvite = false;

														while (!tryingToSwitchInvite)
														{
															while (!int.TryParse(Console.ReadLine(), out switcher))
															{
																Console.WriteLine("Для навигации вводите только номера приглашений");
															}

															if (switcher >= 0 && switcher < listOfUserInviteMessages.listOfUserInviteMessages.Count)
															{
																tryingToSwitchInvite = true;
															}
															else
															{
																Console.WriteLine("Для навигации вводите только номера приглашений");
															}
														}

														listOfUserInviteMessages.listOfUserInviteMessages[switcher].InviteAccepted = true;

														Console.WriteLine($"Приглашение от {listOfUserInviteMessages.listOfUserInviteMessages[switcher].UserName} принято");

														using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\{listOfUserInviteMessages.listOfUserInviteMessages[switcher].UserName}\\ListOfUserInviteMessages.json", FileMode.OpenOrCreate))
														{
															await JsonSerializer.SerializeAsync<ListOfUserInviteMessages>(fileStream, listOfUserInviteMessages);
														}

														flagForExitFromInviteAccepting = true;
													}
												}
												else
												{
													Console.WriteLine("Список приглашений пуст");
												}
												

												break;
											}
										
										case 3:
											{
												flagForExitFromAccount = true;

												break;
											}
									}

																		

									
								}
							}

							break;
						}

					case 3:
						{
							Console.WriteLine("Закрытие чата");

							exitFlag = true;

							break;
						}

					case 4:
						{
							Console.Clear();

							break;
						}
				}
			}

			#region Save list of account to file
			using (FileStream fileStream = new FileStream($"{pathOfEnigmaFolder}\\ListOfUserAccounts.json", FileMode.OpenOrCreate))
			{
				await JsonSerializer.SerializeAsync<UserDataBase>(fileStream, userDataBase);
			}
			#endregion
		}
	}
}
